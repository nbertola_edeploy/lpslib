import { Component, OnInit } from "@angular/core";
import { merge, Observable, Subject } from "rxjs";
import { AlertMessage, LpsUiKitService } from "./lps-ui-kit.service";

@Component({
  selector: "ng-alert",
  template: `
    <div
      class="alert"
      *ngIf="alertMessage$ | async as alertMessage"
      [ngStyle]="{ background: alertMessage.color }"
    >
      <span class="closebtn" (click)="closeAlert()">&times;</span>
      <strong>{{ alertMessage.prefix }}!</strong> {{ alertMessage.message }}
    </div>
  `,
  styleUrls: ["./ng-alert.component.scss"]
})
export class LpsUiKitComponent implements OnInit {
  alertMessage$: Observable<AlertMessage | boolean>;
  close$ = new Subject<boolean>();

  constructor(private alertService: LpsUiKitService) {}

  ngOnInit() {
    this.alertMessage$ = merge(this.alertService.alertMessage$, this.close$);
  }

  closeAlert(): void {
    this.close$.next();
  }
}
