import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LpsUiKitComponent } from "./lps-ui-kit.component";

@NgModule({
  declarations: [LpsUiKitComponent],
  imports: [CommonModule],
  exports: [LpsUiKitComponent]
})
export class LpsUiKitModule {}
