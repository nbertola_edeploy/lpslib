import { TestBed } from '@angular/core/testing';

import { LpsUiKitService } from './lps-ui-kit.service';

describe('LpsUiKitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LpsUiKitService = TestBed.get(LpsUiKitService);
    expect(service).toBeTruthy();
  });
});
