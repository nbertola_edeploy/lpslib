import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpsUiKitComponent } from './lps-ui-kit.component';

describe('LpsUiKitComponent', () => {
  let component: LpsUiKitComponent;
  let fixture: ComponentFixture<LpsUiKitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpsUiKitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpsUiKitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
