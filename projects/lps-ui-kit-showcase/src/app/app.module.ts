import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LpsUiKitService, LpsUiKitComponent, LpsUiKitModule } from 'projects/lps-ui-kit/src/public-api';

@NgModule({
  declarations: [
    AppComponent,
    LpsUiKitComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
